package com.github.soshibby.youtube;

import com.google.gson.annotations.SerializedName;

public class ErrorMessage {
	@SerializedName("errorMessage")
	private String mMessage;
	
	public ErrorMessage(String message){
		mMessage = message;
	}
	
	public String getMessage(){
		return mMessage;
	}
}
