package com.github.soshibby.youtube;

import com.google.gson.annotations.SerializedName;

public class YoutubeStatusMessage {
	
	@SerializedName("propertyName")
	public String mPropertyName;
	
	@SerializedName("propertyValue")
	public Object mPropertyValue;
	
	public String getPropertyName(){
		return mPropertyName;
	}
	
	public Object getPropertyValue(){
		return mPropertyValue;
	}
}
