package com.github.soshibby.youtube;

import java.util.Map;

import org.conductor.component.annotations.Property;
import org.conductor.component.annotations.ReadOnlyProperty;
import org.conductor.component.types.Component;
import org.conductor.component.types.DataType;
import org.conductor.database.Database;

import com.github.soshibby.misc.Assert;
import com.github.soshibby.youtube.server.MessageReceivedEvent;
import com.github.soshibby.youtube.server.MessageReceivedListener;
import com.github.soshibby.youtube.server.YoutubeServer;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@org.conductor.component.annotations.Component(type = "youtube")
@ReadOnlyProperty(name = "volume", dataType = DataType.INTEGER, defaultValue = "0")
@ReadOnlyProperty(name = "duration", dataType = DataType.INTEGER, defaultValue = "0")
@ReadOnlyProperty(name = "currentTime", dataType = DataType.INTEGER, defaultValue = "0")
@ReadOnlyProperty(name = "isMuted", dataType = DataType.BOOLEAN, defaultValue = "false")
@ReadOnlyProperty(name = "videoUri", dataType = DataType.STRING, defaultValue = "")
public class Youtube extends Component implements MessageReceivedListener, Runnable {

	private static final Logger log = LoggerFactory.getLogger(Youtube.class);
	private YoutubeServer server;
	private Thread serverThread;
	
	public Youtube(Database database, Map<String, Object> options) {
		super(database, options);
		log.info("Initializing Youtube component.");
		this.server = new YoutubeServer(9001);
		this.server.addMessageReceivedListener(this);
		
		this.serverThread = new Thread(this);
		this.serverThread.setDaemon(true);
		this.serverThread.start();
	}

	@Override
	public void run() {
		try {
			while(!Thread.currentThread().isInterrupted()){
				server.start();
			}
		} catch (Exception e) {
			reportError(e.getMessage(), ExceptionUtils.getStackTrace(e));
			e.printStackTrace();
			log.error("Server exception occured.", e);
		}

		log.warn("Server stopped.");
	}

	@Property(initialValue = "false")
	public void setPlaying(Boolean playing) {
		JsonObject json = new JsonObject();
		json.addProperty("propertyName", "playing");
		json.addProperty("propertyValue", playing);
		String message = new Gson().toJson(json);
		this.server.broadcast(message, null);
	}
	
	@Property(initialValue = "false")
	public void setFullscreen(Boolean fullscreen) {
		JsonObject json = new JsonObject();
		json.addProperty("propertyName", "fullscreen");
		json.addProperty("propertyValue", fullscreen);
		String message = new Gson().toJson(json);
		this.server.broadcast(message, null);
	}

	//On message received from the chrome extension
	@Override
	public void onMessageReceived(Object sender, MessageReceivedEvent event) {
		try{
			log.info("Incoming message from chrome extension '{}'", event.getMessage());
			YoutubeStatusMessage status = new Gson().fromJson(event.getMessage(), YoutubeStatusMessage.class);
			
			switch(status.getPropertyName()){
				case "playing":
					Assert.isBoolean(status.getPropertyValue(), "Expected property value to be boolean for property name 'playing'");
					boolean isPlaying = (boolean) status.getPropertyValue();
					updatePropertyValue("playing", isPlaying);
					break;
				case "volume":
					int volume;
					
					try{
						 volume = convertToInt(status.getPropertyValue());
					}catch(Exception e){
						throw new Exception("Expected property value to be of type int or double for property with the name 'volume'");
					}
					
					updatePropertyValue("volume", volume);
					break;
				case "duration":
					int duration;
					
					try{
						duration = convertToInt(status.getPropertyValue());
					}catch(Exception e){
						throw new Exception("Expected property value to be of type int or double for property with the name 'duration'");
					}
					
					updatePropertyValue("length", duration);
					break;
				case "currentTime":
					int currentTime = convertToInt(status.getPropertyValue());
					
					try{
						duration = convertToInt(status.getPropertyValue());
					}catch(Exception e){
						throw new Exception("Expected property value to be of type int or double for property with the name 'currentTime'");
					}
					
					updatePropertyValue("time", currentTime);
					break;
				case "isMuted":
					Assert.isBoolean(status.getPropertyValue(), "Expected property value to be of type boolean for property name 'isMuted'");
					boolean isMuted = (boolean) status.getPropertyValue();
					updatePropertyValue("muted", isMuted);
					break;
				case "videoUri":
					Assert.isString(status.getPropertyValue(), "Expected property value to be of type string for property name 'videoUri'");
					String videoUri = (String) status.getPropertyValue();
					updatePropertyValue("videoUri", videoUri);
					break;
				default:
					throw new Exception("Unexpected property name, no property name exists with the name '" + status.getPropertyName() + "'");
			}
		}catch(Exception e){
			//Send error message back to the chrome extension
			log.error("Error when parsing message from chrome extension.", e);
			ErrorMessage message = new ErrorMessage(e.getMessage());
			String json = new Gson().toJson(message);
			event.getClientSocket().getSession().getRemote().sendStringByFuture(json);
			reportError(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	@Override
	public void destroy() {
		try {
			this.server.stop();
		} catch (Exception e) {
			log.error("An error occured while shutting down server.", e);
			reportError(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	private int convertToInt(Object variable) throws Exception{
		if(variable instanceof Double)
			return ((Double) variable).intValue();
		else if(variable instanceof Integer)
			return (int) variable;
		else
			throw new Exception("Unable to convert variable of type '" + variable.getClass().getName() + "' to integer.");
	}

}
