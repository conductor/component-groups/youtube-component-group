package com.github.soshibby.youtube.server;

public interface MessageReceivedListener {
	void onMessageReceived(Object sender, MessageReceivedEvent event);
}
