package com.github.soshibby.youtube.server;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jetty.websocket.servlet.ServletUpgradeRequest;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeResponse;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;

public class CustomWebSocketCreator implements WebSocketCreator {
	private List<SocketCreatedListener> listeners = new ArrayList<SocketCreatedListener>();
	
	@Override
	public Object createWebSocket(ServletUpgradeRequest req, ServletUpgradeResponse resp) {
		ClientSocket socket = new ClientSocket();
		
		for(SocketCreatedListener listener : listeners){
			listener.onSocketCreated(this, socket);
		}
		
		return socket;
	}
	
	public void addSocketCreatedListener(SocketCreatedListener listener){
		this.listeners.add(listener);
	}
	
	public void removeSocketCreatedListener(SocketCreatedListener listener){
		this.listeners.remove(listener);
	}
}
