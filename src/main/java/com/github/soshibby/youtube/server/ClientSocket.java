package com.github.soshibby.youtube.server;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@WebSocket(maxIdleTime=0)
public class ClientSocket {

    private static final Logger log = LoggerFactory.getLogger(YoutubeServer.class);
    private List<MessageReceivedListener> messageReceivedListeners = new ArrayList<MessageReceivedListener>();
    private Session session;
    private String address;
    
    public Session getSession(){
    	return this.session;
    }
    
    public String getAddress(){
    	return this.address;
    }
    
    public void addMessageReceivedListener(MessageReceivedListener listener){
    	this.messageReceivedListeners.add(listener);
    }
    
    public void removeMessageReceivedListener(MessageReceivedListener listener){
    	this.messageReceivedListeners.remove(listener);
    }
    
    @OnWebSocketClose
    public void onClose(Session session, int statusCode, String reason) {
    	log.info("Client disconnected: " + this.address + " (statusCode=" + statusCode + ", reason=" + reason + ")");
    }

    @OnWebSocketError
    public void onError(Session session, Throwable t) {
        log.warn("Client error: " + this.address + ".", t);
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        this.session = session;
        this.address = this.session.getRemoteAddress().getAddress().toString().substring(1);

        log.info("New client connection from " + this.address + ".");
    }

    @OnWebSocketMessage
    public void onMessage(Session session, String message) {
        log.info("Received message from client " + this.address + " " + message);
        
    	for(MessageReceivedListener listener : messageReceivedListeners){
    		MessageReceivedEvent event = new MessageReceivedEvent(this, this, message);
    		listener.onMessageReceived(this, event);
    	}
    }
}
