package com.github.soshibby.youtube.server;

public interface SocketCreatedListener {
    void onSocketCreated(Object sender, ClientSocket socket);
}
