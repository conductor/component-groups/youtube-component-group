package com.github.soshibby.youtube.server;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class YoutubeServer implements MessageReceivedListener {

	private static final Logger log = LoggerFactory.getLogger(YoutubeServer.class);
	private List<ClientSocket> clients = new CopyOnWriteArrayList<>();
	private List<MessageReceivedListener> messageReceivedListeners = new ArrayList<MessageReceivedListener>();
    private Server server;
	private int listeningPort;
	
    public YoutubeServer(int listeningPort) {
    	this.listeningPort = listeningPort;
    }
    
    public void start() throws Exception{
    	server = new Server(this.listeningPort);
        
		WebSocketHandler wsHandler = new WebSocketHandler() {
            
			@Override
            public void configure(WebSocketServletFactory factory) {
            	CustomWebSocketCreator socketCreator = new CustomWebSocketCreator();
            	
            	socketCreator.addSocketCreatedListener((sender, socket) -> addClient(socket));
            	
                factory.setCreator(socketCreator);
            }

        };
        
        log.info("Starting server on port " + this.listeningPort + ".");
        this.server.setHandler(wsHandler);
        this.server.start();
        this.server.join();
    }
    
    public void stop() throws Exception{
    	this.server.stop();
    }
    
    public void addClient(ClientSocket client) {
		log.info("New websocket client added.");
        this.clients.add(client);
        client.addMessageReceivedListener(this);
    }

    public void removeClient(ClientSocket client) {
        this.clients.remove(client);
        //this.broadcast("disconnect " + client.id, client);  
    }

    public void broadcast(String message, ClientSocket excludedClient) {
		log.debug("Broadcasting to all clients: " + message);
    	
    	for(ClientSocket client : this.clients )
    	{
    		if (!client.equals(excludedClient) && client.getSession().isOpen() )
    		{
    			try {
    				client.getSession().getRemote().sendStringByFuture(message);
    			} catch (Exception e) {
					log.error("Error when broadcasting to " + client.getAddress() + ".", e);
				}
    		}
    	}
    }
    
	@Override
	public void onMessageReceived(Object sender, MessageReceivedEvent event) {
		for(MessageReceivedListener listener : this.messageReceivedListeners){
    		listener.onMessageReceived(this, event);
    	}
	}
	
	public void addMessageReceivedListener(MessageReceivedListener listener){
		this.messageReceivedListeners.add(listener);
    }
	
	public void removeMessageReceivedListener(MessageReceivedListener listener){
		this.messageReceivedListeners.remove(listener);
    }
}
