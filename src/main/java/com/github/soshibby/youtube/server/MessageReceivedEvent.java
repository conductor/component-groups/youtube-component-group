package com.github.soshibby.youtube.server;

import java.util.EventObject;

public class MessageReceivedEvent extends EventObject {
	private String message;
	private ClientSocket clientSocket;
	
	public MessageReceivedEvent(Object source, ClientSocket clientSocket, String message) {
		super(source);
		this.message = message;
		this.clientSocket = clientSocket;
	}
	
	public String getMessage(){
		return this.message;
	}
	
	public ClientSocket getClientSocket(){
		return this.clientSocket;
	}
}
