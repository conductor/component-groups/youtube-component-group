package com.github.soshibby.misc;

import java.net.MalformedURLException;
import java.net.URL;

public class Assert {
	public static boolean isValidURL(String url){
		try{
			new URL(url);
		}catch(MalformedURLException urlException){
			return false;
		}
		
		return true;
	}
	
	public static void isBoolean(Object variable, String throwMessageOnError) throws Exception{
		if(!(variable instanceof Boolean))
			throw new Exception("Assert failed: " + throwMessageOnError);
	}
	
	public static void isString(Object variable, String throwMessageOnError) throws Exception{
		if(!(variable instanceof String))
			throw new Exception("Assert failed: " + throwMessageOnError);
	}
}
